import java.util.ArrayList;

public class BinTree implements Comparable{

    private int data;
//    private int real;
    private BinTree left;
    private BinTree right;
    boolean isLeaf;
    int key;
//    int n;
    ArrayList<Integer> pref;

//    public BinTree( int n){
//        ArrayList<Integer> pref1 = new ArrayList<>();
//        pref1.add(0);
//        ArrayList<Integer> pref2 = new ArrayList<>();
//        pref2.add(1);
//        this.left = new BinTree(n - 1,0,pref1);
//        this.right = new BinTree(n - 1, (int)Math.pow(2,n-1),pref2);
//        this.n = n;
//    }
//
//    public BinTree(int n, int x, ArrayList<Integer> j){
//        this.n = n;
//        if(n==0) {
//            this.pref = j;
//            this.data = x;
//            this.isLeaf = true;
//        }else{
//            this.pref = j;
//            ArrayList<Integer> pref1 = new ArrayList<>(pref);
//            pref1.add(0);
//            ArrayList<Integer> pref2 = new ArrayList<>(pref);
//            pref2.add(1);
//            this.isLeaf = false;
//            this.left = new BinTree(n - 1, x,pref1);
//            this.right = new BinTree(n - 1, x + (int)Math.pow(2,n-1),pref2);
//        }
//    }

    public BinTree(BinTree left , BinTree right){
        this.left = left;
        this.right = right;
        this.isLeaf = false;
        this.data = left.getData() + right.getData();
    }

    public BinTree(int key, int data){
        this.isLeaf = true;
        this.key = key;
        this.data = data;
    }



    public int getData() {
        return data;
    }

    public ArrayList<Integer> getPref() {
        return pref;
    }

//    public ArrayList<Integer> allData(){
//        ArrayList<Integer> ar = new ArrayList<>();
//
//        if(isLeaf) { ar.add(getData()); return ar;}
//        else{
//            ar.addAll(getLeft().allData());
//            ar.addAll(getRight().allData());
//        }
//
//        return ar;
//    }

    public ArrayList<Integer> allData(){
        ArrayList<Integer> ar = new ArrayList<>();
        if(isLeaf) { ar.add(getData()); return ar;}
        else{
            if(left != null )ar.addAll(getLeft().allData());
            if(right != null )ar.addAll(getRight().allData());
        }
        return ar;
    }

    public ArrayList<ArrayList<Integer>> allPrefs(){
        ArrayList<ArrayList<Integer>> ar = new ArrayList<>();

        if(isLeaf) { ArrayList<ArrayList<Integer>> arr = new ArrayList<>();arr.add(getPref());return arr;}
        else{
            ar.addAll(getLeft().allPrefs());
            ar.addAll(getRight().allPrefs());
        }

        return ar;
    }

//    public ArrayList<Integer> allReal(){
//        ArrayList<Integer> ar = new ArrayList<>();
//
//        if(isLeaf) { ar.add(getReal()); return ar;}
//        else{
//            ar.addAll(getLeft().allReal());
//            ar.addAll(getRight().allReal());
//        }
//
//        return ar;
//    }

//    public void setReal(int key, int r){
//        if(isLeaf){
//            if(data == key) real = r;
//        }else{
//            left.setReal(key, r);
//            right.setReal(key, r);
//        }
//    }
//
//    public int getReal() {
//        return real;
//    }

    public BinTree getLeft() {
        return left;
    }

    public BinTree getRight() {
        return right;
    }


    @Override
    public int compareTo(Object o) {
        BinTree tmp = (BinTree)o;
        if(this.data < tmp.data){
            return -1;
        }
        if(this.data == tmp.data){
            return 0;
        }
        if(this.data > tmp.data){
            return 1;
        }

        return 0;
    }
}
